import java.io.IOException;

public class LibraryWrapper extends ODCUWrapper{

    /**
     * Constructor
     *
     * @param apiId  the apiID
     * @param apiKey the apiKey
     */
    public LibraryWrapper(String apiId, String apiKey) {
        super(apiId, apiKey);
    }

    // TODO: Add overloaded method to accept a Date instead of a string
    public String getHours(String date) throws IOException, InterruptedException {
        String apiPath = "/hours/" + date;
        return this.getRequest(apiPath);
    }

    public String getEvents() throws IOException, InterruptedException {
        String apiPath = "/events/";
        return this.getRequest(apiPath);
    }

    public String getComputers() throws IOException, InterruptedException {
        String apiPath = "/computers/";
        return this.getRequest(apiPath);
    }

    public String getOccupancy() throws IOException, InterruptedException {
        String apiPath = "/occupancy/";
        return this.getRequest(apiPath);
    }

    public String getRoomsList() throws IOException, InterruptedException {
        String apiPath = "/rooms/getRoomsList/";
        return this.getRequest(apiPath);
    }

    public String getRoomsReservations(String resourceId, String scheduleId) throws IOException, InterruptedException {
        String apiPath = "/rooms/getRoomsReservations/" + resourceId + "/" + scheduleId;
        return this.getRequest(apiPath);
    }
}