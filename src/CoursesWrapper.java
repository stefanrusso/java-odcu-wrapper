import java.io.IOException;

public class CoursesWrapper extends ODCUWrapper {
    /**
     * Constructor
     *
     * @param apiId  the apiID
     * @param apiKey the apiKey
     */
    public CoursesWrapper(String apiId, String apiKey) {
        super(apiId, apiKey);
    }


    /**
     * @param subject
     * @param catalog
     * @param career
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String getCourse(String subject, String catalog, String career) throws IOException, InterruptedException {
        String apiPath = "/catalog/filter/" + subject + "/" + catalog + "/" + career;
        return this.getRequest(apiPath);
    }

    public String getDescription(String courseID) throws IOException, InterruptedException {
        String apiPath = "/description/filter/" + courseID;
        return this.getRequest(apiPath);
    }

    public String getSection(String subject, String catalog) throws IOException, InterruptedException {
        String apiPath = "/section/filter/" + subject + "/" + catalog;
        return this.getRequest(apiPath);
    }

    public String getSchedule(String courseId, String subject, String catalog) throws IOException, InterruptedException {
        String apiPath = "/schedule/filter/" + courseId + "/" + subject + "/" + catalog;
        return this.getRequest(apiPath);
    }

    public String getSession(String career, String termcode, String sessioncode) throws IOException, InterruptedException {
        String apiPath = "/session/filter/" + career + "/" + termcode + "/" + sessioncode;
        return this.getRequest(apiPath);
    }

    public String getFaculty(String facultyCode, String departmentCode) throws IOException, InterruptedException {
        String apiPath = "/faculty/filter/" + facultyCode + "/" + departmentCode;
        return this.getRequest(apiPath);
    }


}
