import java.io.IOException;


public class FacilitiesWrapper extends ODCUWrapper{
    /**
     * Constructor
     *
     * @param apiId  the apiID
     * @param apiKey the apiKey
     */
    public FacilitiesWrapper(String apiId, String apiKey) {
        super(apiId, apiKey);
    }

    public String getPointList() throws IOException, InterruptedException {
        String apiPath = "/pointlist/";
        return this.getRequest(apiPath);
    }

    public String getBuildingList() throws IOException, InterruptedException {
        String apiPath = "/buildinglist/";
        return this.getRequest(apiPath);
    }

    // TODO: Add overloaded method to accept a Date instead of a string
    public String getConsumption(String startTime, String endTime) throws IOException, InterruptedException {
        String apiPath = "/consumption/filter" + startTime + "/" + endTime;
        return this.getRequest(apiPath);
    }

    // TODO: Add overloaded method to accept a Date instead of a string
    public String getEnvironmental(String startTime, String endTime) throws IOException, InterruptedException {
        String apiPath = "/environmental/filter" + startTime + "/" + endTime;
        return this.getRequest(apiPath);
    }


}