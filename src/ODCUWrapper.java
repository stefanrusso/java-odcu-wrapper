import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

abstract class ODCUWrapper {
    private String apiBaseURL = "https://opendata.concordia.ca/API/v1";
    protected HttpClient httpClient = null;

    /**
     * Constructor
     *
     * @param apiId  the apiID
     * @param apiKey the apiKey
     */
    public ODCUWrapper(String apiId, String apiKey) {
        this.httpClient = buildClient(apiId, apiKey);
    }

    private ODCUWrapper() {
        // DO nothing
    }

    /**
     * Getter for apiBaseUrl
     * @return A string containing the apiBaseUrl
     */
    public String getApiBaseURL() {
        return apiBaseURL;
    }

    /**
     * Sets the apiBaseUrl to the one provided
     * @param apiBaseURL String with the apiBaseUrl
     */
    public void setApiBaseURL(String apiBaseURL) {
        // TODO: Validate the new apiBaseUrl
        this.apiBaseURL = apiBaseURL;
    }

    /**
     * Getter for httpClient
     * @return HttpClient instance of httpClient
     */
    public HttpClient getHttpClient() {
        return httpClient;
    }


    /**
     * Creates an Authenticator object using the supplied apiId, and apiKey
     *
     * @param apiId  String containing the apiId
     * @param apiKey String containing the apiKey
     * @return Authenticator constructed using the supplied apiId, and apiKey
     */
    private Authenticator createAuthenticator(String apiId, String apiKey) {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(apiId, apiKey.toCharArray());
            }
        };
    }

    /**
     * Builds the httpClient and returns it
     *
     * @param apiId  String containing the apiId
     * @param apiKey String containing the apiKey
     * @return httpClient object ready for use in sending requests
     */
    private HttpClient buildClient(String apiId, String apiKey) {
        return HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .authenticator(createAuthenticator(apiId, apiKey))
                .build();
    }

    /**
     * Sends the request to the API, returns the JSON response as a string
     * @param urlPath the API path to concatenate with the base URL
     * @return the response body if it was a successful call
     * @throws IOException
     * @throws InterruptedException
     */
    protected String getRequest(String urlPath) throws IOException, InterruptedException {
        String completeUrl = this.apiBaseURL + urlPath;
        // TODO: Validate completeUrl
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(completeUrl))
                .GET()
                .build();
        HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

}
